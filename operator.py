class Operator:
    """operators"""
    def __init__(self):
        self.value = 0
    
    def add(self, value):
        self.value = self.value + value
    
    def sub(self, value):
        self.value = self.value - value
    
    def mul(self, value):
        self.value = self.value * value
    
    def div(self, value):
        self.value = self.value / value
    
    def sqr(self):
        self.value = self.value * self.value
    
    def get(self):
        return self.value
    
    def parse(self, string):
        if string.strip() == 'SQR':
            self.sqr()
        else:
            operation, value = string.strip().split()
            value = int(value)
            if operation == 'ADD':
                self.add(value)
            elif operation == 'SUB':
                self.sub(value)
            elif operation == 'MUL':
                self.mul(value)
            elif operation == 'DIV':
                self.div(value)
            else:
                print 'Operation not recognised. Line -' + string + '- was skipped.'
    
    def display(self):
        print 'Value is ' + str(self.value)
        
