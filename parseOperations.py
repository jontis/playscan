#! /usr/bin/env python3.2

# purpose of this script: parse operations and values from an input file, perform
# the operations and display the final result

# import core libraries
import os
import sys

# import operator class
import operator

# parse command line for filename
filename = sys.argv[1]
operations_file = open(filename)
calculator = operator.Operator()

for line in operations_file:
    calculator.parse(line)

operations_file.close()

calculator.display()
